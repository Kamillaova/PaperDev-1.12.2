package net.minecraft.server.v1_12_R1;

abstract class NBTNumber extends NBTBase {

    protected NBTNumber() {}

    public abstract long d();

    public abstract int e();

    public abstract short f();

    public abstract byte g();

    public abstract double asDouble();

    public abstract float i();
}
