package net.minecraft.server.v1_12_R1;

public enum EnumBlockFaceShape {

    SOLID, BOWL, CENTER_SMALL, MIDDLE_POLE_THIN, CENTER, MIDDLE_POLE, CENTER_BIG, MIDDLE_POLE_THICK, UNDEFINED;

    private EnumBlockFaceShape() {}
}
