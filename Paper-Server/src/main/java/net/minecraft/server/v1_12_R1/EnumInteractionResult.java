package net.minecraft.server.v1_12_R1;

public enum EnumInteractionResult {

    SUCCESS, PASS, FAIL;

    private EnumInteractionResult() {}
}
