package net.minecraft.server.v1_12_R1;

public enum EnumAnimation {

    NONE, EAT, DRINK, BLOCK, BOW;

    private EnumAnimation() {}
}
