package net.minecraft.server.v1_12_R1;

import javax.annotation.Nullable;

public interface IAttribute {

    String getName();

    double a(double d0);

    double getDefault();

    boolean c();

    @Nullable
    IAttribute d();
}
