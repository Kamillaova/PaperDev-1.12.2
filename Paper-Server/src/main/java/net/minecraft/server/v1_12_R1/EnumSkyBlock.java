package net.minecraft.server.v1_12_R1;

public enum EnumSkyBlock {

    SKY(15), BLOCK(0);

    public final int c;

    private EnumSkyBlock(int i) {
        this.c = i;
    }
}
