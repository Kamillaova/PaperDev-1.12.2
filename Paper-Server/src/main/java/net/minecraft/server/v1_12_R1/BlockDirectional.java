package net.minecraft.server.v1_12_R1;

public abstract class BlockDirectional extends Block {

    public static final BlockStateDirection FACING = BlockStateDirection.of("facing");

    protected BlockDirectional(Material material) {
        super(material);
    }
}
