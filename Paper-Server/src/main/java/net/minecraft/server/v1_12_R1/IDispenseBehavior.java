package net.minecraft.server.v1_12_R1;

public interface IDispenseBehavior {

    IDispenseBehavior NONE = new IDispenseBehavior() {
        public ItemStack a(ISourceBlock isourceblock, ItemStack itemstack) {
            return itemstack;
        }
    };

    ItemStack a(ISourceBlock isourceblock, ItemStack itemstack);
}
