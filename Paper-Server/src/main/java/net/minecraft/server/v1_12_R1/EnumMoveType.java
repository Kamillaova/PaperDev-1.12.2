package net.minecraft.server.v1_12_R1;

public enum EnumMoveType {

    SELF, PLAYER, PISTON, SHULKER_BOX, SHULKER;

    private EnumMoveType() {}
}
