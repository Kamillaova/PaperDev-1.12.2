package net.minecraft.server.v1_12_R1;

public class InventoryHorseChest extends InventorySubcontainer {

    // CraftBukkit start
    public InventoryHorseChest(String s, int i, EntityHorseAbstract owner) {
        super(s, false, i, (org.bukkit.entity.AbstractHorse) owner.getBukkitEntity());
        // CraftBukkit end
    }
}
