package net.minecraft.server.v1_12_R1;

public class BlockRedFlowers extends BlockFlowers {

    public BlockRedFlowers() {}

    public BlockFlowers.EnumFlowerType e() {
        return BlockFlowers.EnumFlowerType.RED;
    }
}
