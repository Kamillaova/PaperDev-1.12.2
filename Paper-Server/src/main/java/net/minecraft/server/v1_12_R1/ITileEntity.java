package net.minecraft.server.v1_12_R1;

import javax.annotation.Nullable;

public interface ITileEntity {

    @Nullable
    TileEntity a(World world, int i);
}
