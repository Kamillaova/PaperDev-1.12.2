package net.minecraft.server.v1_12_R1;

public class ExceptionUsage extends ExceptionInvalidSyntax {

    public ExceptionUsage(String s, Object... aobject) {
        super(s, aobject);
    }

    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
