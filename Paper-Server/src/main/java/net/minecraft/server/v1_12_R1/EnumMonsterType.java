package net.minecraft.server.v1_12_R1;

public enum EnumMonsterType {

    UNDEFINED, UNDEAD, ARTHROPOD, ILLAGER;

    private EnumMonsterType() {}
}
