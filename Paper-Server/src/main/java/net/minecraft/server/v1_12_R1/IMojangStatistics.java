package net.minecraft.server.v1_12_R1;

public interface IMojangStatistics {

    void a(MojangStatisticsGenerator mojangstatisticsgenerator);

    void b(MojangStatisticsGenerator mojangstatisticsgenerator);

    boolean getSnooperEnabled();
}
