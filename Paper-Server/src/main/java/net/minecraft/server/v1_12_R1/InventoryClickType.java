package net.minecraft.server.v1_12_R1;

public enum InventoryClickType {

    PICKUP, QUICK_MOVE, SWAP, CLONE, THROW, QUICK_CRAFT, PICKUP_ALL;

    private InventoryClickType() {}
}
