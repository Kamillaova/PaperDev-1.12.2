package net.minecraft.server.v1_12_R1;

public class BlockYellowFlowers extends BlockFlowers {

    public BlockYellowFlowers() {}

    public BlockFlowers.EnumFlowerType e() {
        return BlockFlowers.EnumFlowerType.YELLOW;
    }
}
