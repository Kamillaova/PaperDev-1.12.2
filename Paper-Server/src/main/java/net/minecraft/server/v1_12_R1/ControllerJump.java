package net.minecraft.server.v1_12_R1;

public class ControllerJump {

    private final EntityInsentient b;
    protected boolean a;

    public ControllerJump(EntityInsentient entityinsentient) {
        this.b = entityinsentient;
    }

    public void a() {
        this.a = true;
    }

    public void jumpIfSet() { this.b(); } // Paper - OBFHELPER
    public void b() {
        this.b.l(this.a);
        this.a = false;
    }
}
