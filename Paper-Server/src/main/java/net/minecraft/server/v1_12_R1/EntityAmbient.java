package net.minecraft.server.v1_12_R1;

public abstract class EntityAmbient extends EntityInsentient implements IAnimal {

    public EntityAmbient(World world) {
        super(world);
    }

    public boolean a(EntityHuman entityhuman) {
        return false;
    }
}
