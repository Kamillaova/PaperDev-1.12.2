package net.minecraft.server.v1_12_R1;

import javax.annotation.Nullable;

public interface IPlayerFileData {

    void save(EntityHuman entityhuman);

    @Nullable
    NBTTagCompound load(EntityHuman entityhuman);

    String[] getSeenPlayers();
}
