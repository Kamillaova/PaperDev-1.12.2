package net.minecraft.server.v1_12_R1;

public interface IPosition {

    double getX();

    double getY();

    double getZ();
}
