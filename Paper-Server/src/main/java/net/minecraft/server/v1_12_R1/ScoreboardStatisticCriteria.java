package net.minecraft.server.v1_12_R1;

public class ScoreboardStatisticCriteria extends ScoreboardBaseCriteria {

    private final Statistic o;

    public ScoreboardStatisticCriteria(Statistic statistic) {
        super(statistic.name);
        this.o = statistic;
    }
}
