package net.minecraft.server.v1_12_R1;

public class ExceptionWorldConflict extends Exception {

    public ExceptionWorldConflict(String s) {
        super(s);
    }
}
