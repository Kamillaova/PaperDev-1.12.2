package net.minecraft.server.v1_12_R1;

public interface INamableTileEntity {

    String getName();

    boolean hasCustomName();

    IChatBaseComponent getScoreboardDisplayName();
}
