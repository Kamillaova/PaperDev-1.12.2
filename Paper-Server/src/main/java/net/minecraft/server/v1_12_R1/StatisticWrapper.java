package net.minecraft.server.v1_12_R1;

public class StatisticWrapper {

    private int a;
    private IJsonStatistic b;

    public StatisticWrapper() {}

    public int a() {
        return this.a;
    }

    public void a(int i) {
        this.a = i;
    }

    public <T extends IJsonStatistic> T b() {
        // Kamillaova: add cast to T, fix build
        return (T) this.b;
    }

    public void a(IJsonStatistic ijsonstatistic) {
        this.b = ijsonstatistic;
    }
}
