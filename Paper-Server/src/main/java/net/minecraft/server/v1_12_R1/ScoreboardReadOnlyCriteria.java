package net.minecraft.server.v1_12_R1;

public class ScoreboardReadOnlyCriteria extends ScoreboardBaseCriteria {

    public ScoreboardReadOnlyCriteria(String s) {
        super(s);
    }

    public boolean isReadOnly() {
        return true;
    }
}
