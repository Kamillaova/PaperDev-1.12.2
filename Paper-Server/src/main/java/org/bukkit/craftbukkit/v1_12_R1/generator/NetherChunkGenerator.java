package org.bukkit.craftbukkit.v1_12_R1.generator;

import net.minecraft.server.v1_12_R1.World;

/**
 * This class is useless. Just fyi.
 */
public class NetherChunkGenerator extends NormalChunkGenerator {
    public NetherChunkGenerator(World world, long seed) {
        super(world, seed);
    }
}
