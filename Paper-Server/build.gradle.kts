import com.github.jengelman.gradle.plugins.shadow.transformers.Log4j2PluginsCacheFileTransformer

plugins {
  alias(libs.plugins.shadow)
}

dependencies {
  api(project(":Paper-API"))

  api(libs.log4j.api)
  implementation(libs.log4j.core)
  annotationProcessor(libs.log4j.core)
  implementation(libs.log4j.iostreams)
  implementation(libs.log4j.slf4j.impl)
  runtimeOnly(libs.disruptor)

  api(libs.netty.buffer)
  api(libs.netty.codec)
  api(libs.netty.handler)
  api(libs.netty.transport)
  api(libs.netty.transport.classes.epoll)
  runtimeOnly(libs.netty.transport.native.epoll) { artifact { classifier = "linux-x86_64" } }
  runtimeOnly(libs.netty.transport.native.epoll) { artifact { classifier = "linux-aarch_64" } }

  implementation(libs.jopt.simple)
  implementation(libs.terminalconsoleappender)
  implementation(libs.json.simple)
  implementation(libs.snakeyaml)

  implementation(libs.trove4j)

  runtimeOnly(libs.jline.terminal.jansi)

  constraints {
    implementation(libs.jline.reader)
  }
}

tasks {
  assemble { dependsOn(shadowJar) }
  shadowJar {
    archiveFileName.set("Paper-${project.version}.jar")

    mergeServiceFiles()
    transform(Log4j2PluginsCacheFileTransformer())

    exclude("**/module-info.class")

    manifest {
      attributes(
        "Main-Class" to "org.bukkit.craftbukkit.Main",
        "Specification-Title" to "Bukkit",
        "Implementation-Title" to "CraftBukkit",
        "Implementation-Version" to project.version,
        "Implementation-Vendor" to project.version,
        "Specification-Version" to "unknown",
        "Specification-Vendor" to "Bukkit Team",
        "Multi-Release" to "true"
      )

      arrayOf("net", "com", "org").forEach {
        attributes("$it/bukkit/", "Sealed" to true)
      }
    }
  }

  test {
    filter {
      excludeTestsMatching("org.bukkit.craftbukkit.v1_12_R1.inventory.ItemStack*Test")
    }
  }
}

publishing {
  publications {
    create<MavenPublication>("maven") {
      artifactId = "paper"
      from(components["java"])
    }
  }
}
