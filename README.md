# Clean papermc 1.12.2 dev env without patch system

There are only patches of decompilation fixes (aka "mc-dev fixes"), they are all marked with comments that start with Kamillaova:

### See all patches:

__Unix:__

```console
[u@h Paper-Server]$ grep -r Kamillaova
```

__Windows:__

```console
C:\PaperDev\Paper-Server> findstr /s /i Kamillaova *.*
```

# ATTENTION

# ALL versions of libraries have been updated, and some libraries are not exists, because they are not used (but plugins can use them)
