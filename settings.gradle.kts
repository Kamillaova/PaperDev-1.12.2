@file:Suppress("UnstableApiUsage")

dependencyResolutionManagement {
  repositories {
    mavenCentral()
    maven("https://libraries.minecraft.net") {
      content { includeGroup("com.mojang") }
    }
    maven("https://repo.papermc.io/repository/maven-public/") {
      content { includeGroup("net.md-5") }
    }
  }

  repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
}

rootProject.name = "PaperDev"

include(
  "Paper-API",
  "Paper-Server",
)
